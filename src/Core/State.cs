using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace Flixna.Core {
    public abstract class State
    {
        protected readonly List<Entity> children;
        public int ChildCount => children.Count;
        protected readonly SpriteBatch spriteBatch;
        public State(GraphicsDevice graphics) {
            children = new List<Entity>();
            spriteBatch = new SpriteBatch(graphics);
        }

        public abstract void LoadContent(ContentManager content);
        public void AddChild(Entity entity) => children.Add(entity);
        public void AddChildren(IEnumerable<Entity> children) => this.children.AddRange(children);
        public virtual void Draw(GameTime gameTime) => this.children.ForEach(c => c.Draw(spriteBatch, gameTime));
        public virtual void Update(GameTime gameTime) => this.children.ForEach(c => c.Update(gameTime));
    }
}
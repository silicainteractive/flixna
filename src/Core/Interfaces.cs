using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Flixna.Core {
    public interface IDrawable {
        void Draw(SpriteBatch spriteBatch, GameTime gameTime);
    }
    
    public interface IUpdateable {
        void Update(GameTime gameTime);
    }
}
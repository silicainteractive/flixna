using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Flixna.Core {
    public abstract class Entity : IDrawable, IUpdateable
    {
        public abstract Vector2 Position {get;}
        public abstract void Draw(SpriteBatch spriteBatch, GameTime gameTime);
        public abstract void Update(GameTime gameTime);
    }
}
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using XnaGame = Microsoft.Xna.Framework.Game;

namespace Flixna.Core
{
    public class Game : XnaGame
    {
        GraphicsDeviceManager graphics;
        SpriteBatch? spriteBatch;

        private State currentState;
        public Game()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
            currentState = new DefaultState(GraphicsDevice);
        }

        public void SetState(State state)
        {
            state.LoadContent(this.Content);
            currentState = state;
        }

        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            currentState.LoadContent(this.Content);

            // TODO: use this.Content to load your game content here
            
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();
            
            // TODO: Add your update logic here

            base.Update(gameTime);
            currentState.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here

            base.Draw(gameTime);
            currentState.Draw(gameTime);
        }
    }

    internal sealed class DefaultState : State
    {
        public DefaultState(GraphicsDevice graphics) : base(graphics) {
        }

        public override void Draw(GameTime gameTime)
        {

        }

        public override void LoadContent(ContentManager graphics)
        {   
        }

        public override void Update(GameTime gameTime)
        {
        }
    }
}

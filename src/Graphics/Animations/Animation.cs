using System.Collections.Generic;
using System;
using Flixna.Core;
using Microsoft.Xna.Framework;

namespace Flixna.Graphics.Animations {
    public class Animation : Core.IUpdateable {
        private record AnimationFrame {
            public int frameTimeMs;
            public Rectangle sourceRect;
        }
        
        public bool IsRunning {get; private set;}
        private AnimationFrame CurrentFrame {get; set;}
        public readonly string Name;
        private readonly List<AnimationFrame> frames;
        private int currentFrameIndex;
        public Animation(string name) {
            this.Name = name;
            frames = new List<AnimationFrame>();
            CurrentFrame = new AnimationFrame { frameTimeMs = 0, sourceRect = new Rectangle()};
        }

        public Rectangle FrameRect => CurrentFrame.sourceRect;

        public void Step() { 
            CurrentFrame = frames[currentFrameIndex++];
            if(currentFrameIndex == frames.Count)
                currentFrameIndex = 0;
        }

        public void GenerateFrames(int frameMs, int frameCount, Point frameSize) {
            static Point scalePoint(Point p, int scalar) => (p.ToVector2() * scalar).ToPoint();
            for(int i = 0; i < frameCount; i++){
                var location = scalePoint(frameSize,i);
                frames.Add(new AnimationFrame{
                    frameTimeMs = frameMs,
                    sourceRect = new(location, frameSize)
                });
            }
        }
        public void Reset() {
            currentFrameIndex = 0;
            Step(); 
        }
        public void Play() {
            IsRunning = true;
            Step();
        }
        public void Pauze() {
            IsRunning = false;
        }
        public void Stop() {
            IsRunning = false;
            Reset();
        }

        public void Update(Microsoft.Xna.Framework.GameTime gameTime) {
            if(!IsRunning) return;
            if(gameTime.ElapsedGameTime.Milliseconds > CurrentFrame.frameTimeMs)
                Step();
        }
    }
}
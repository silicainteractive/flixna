using System.Collections.Generic;
using Flixna.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Flixna.Graphics.Animations;

namespace Flixna.Graphics {
    public class AnimatedSprite : Sprite {

        private Dictionary<string, Animation> anims;
        public AnimatedSprite(Entity parent, Texture2D texture) : base(parent, texture) {

        }

        public override void Draw(SpriteBatch spriteBatch, GameTime gameTime) {
            
        }
        public override void Update(GameTime gameTime) {
            base.Update(gameTime);
        }
    }

}
using System;
using Flixna.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Flixna.Graphics {
    public class Sprite: Core.IDrawable, Core.IUpdateable {
        private readonly Texture2D texture;
        private readonly Entity parent;
        public Sprite(Entity parent, Texture2D texture){
            this.parent = parent;
            this.texture = texture;
        }

        public virtual void Update(GameTime gameTime) {
        }
        public virtual void Draw(SpriteBatch spriteBatch, GameTime gameTime) {
            spriteBatch.Draw(texture,parent.Position,null,Color.White);
        }
    }
}
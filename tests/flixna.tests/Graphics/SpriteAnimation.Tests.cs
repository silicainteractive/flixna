using Microsoft.Xna.Framework;
using Xunit;

namespace Flixna.Graphics.Tests {
    public class SpriteAnimTests {


        [Fact]
        public void foo(){
            var g = new GameTime();
            g.ElapsedGameTime = new System.TimeSpan(0,0,0,0,100);
            Assert.Equal(g.ElapsedGameTime.Milliseconds, 100);
        }        
    }
}
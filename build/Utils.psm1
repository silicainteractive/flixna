Function Get-Semver([string] $path) {
    & "gitversion" $path "/showvariable" "semver"
}

Export-ModuleMember -Function "*-*"
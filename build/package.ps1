[CmdletBinding()]
Param(
    [switch]$Clean
)
Import-Module (Join-Path $PSScriptRoot "Utils.psm1")

$projectRoot = Split-Path $PSScriptRoot -Parent

$version = Get-Semver $projectRoot

$libPath = Join-Path -Path $projectRoot -ChildPath "src\flixna.csproj" | Resolve-Path -Relative
$outputPath = Join-Path -Path $projectRoot -ChildPath ".\dist"

If($Clean -and (Test-Path $outputPath)) {
    Remove-Item -Path $outputPath
}

$arguments = @("pack", $libPath)

$options = @{
    "-output" = $outputPath
    "-configuration" = "Release"
    "-runtime" = "win10-x86"
    "p" = "PackageVersion=$version"
}

& "dotnet" @arguments @options